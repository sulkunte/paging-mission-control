import java.sql.Timestamp;

class Data {
	
	protected boolean alert;
	protected int counter;
	protected String component;
	protected Timestamp time;
	protected int satId;

	public Data(String comp, Timestamp timestamp, boolean alert, int count, int id) {
		
		component = comp;
		time = timestamp;
		this.alert = alert;
		counter = count;
		satId = id;
	}
	
	public String getComp() {
		
		return component;
	}
}
