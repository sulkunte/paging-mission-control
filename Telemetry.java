import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;  
import com.google.gson.Gson;

public class Telemetry {

	final static String inputFile = "TestInput"; //set desired file-path

	/* constants defined to indicate location of each field
	 * in the input data
	 */
	final static int time = 0;
	final static int satelliteId = 1;
	final static int redHigh = 2;
	final static int redLow = 5;
	final static int component = 7; 
	final static int rawValue = 6; 
	
	private static ArrayList<Data> dataList = new ArrayList<Data>();
	
	//method returns the value at a particular position in the string
	public static String readValue(int position, String line) {
		
	    String [] data = line.split("\\|");
		return data[position];
	}
	
	public static Timestamp absoluteTime(String time) {
		
		try {
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss.SSS");
		    Date parsedDate = dateFormat.parse(time);
		    Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime()); 
		    return timestamp;
		}
		catch (ParseException e) {
		      
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void checkValues(String line) {
		
		String comp = readValue(component, line); //determine component type
		double value = Double.parseDouble(readValue(rawValue, line));
		boolean valid = true;
		Timestamp currentTime = absoluteTime(readValue(time, line));
		int satId = Integer.parseInt(readValue(satelliteId, line));

		if(comp.equals("BATT")) {
			
			double limit = Double.parseDouble(readValue(redLow, line));
			
			if(value < limit) {
				
				valid = false;
			}
		}
		else { //comp == "TSTAT"
			
			double limit = Double.parseDouble(readValue(redHigh, line));
			if(value > limit) {
				
				valid = false;
			}
		}
		
		Data current = new Data(comp, currentTime, valid, 0, satId);
		dataList.add(current);
	}

	public static void main(String [] args) {
		
		Scanner input = null;	
		
		try {
			
			input = new Scanner(new File(inputFile));
			
			while(input.hasNextLine()) {
				
				String currentLine = input.nextLine();
				
				checkValues(currentLine); //populates array list of data
			}
		}
		
		catch(IOException e) {
			
			e.printStackTrace();
		}
		
		input.close();
		
		for(int element = 0; element < dataList.size(); element++) {

			if(dataList.get(element).alert != true) {
				
				for(int i = dataList.indexOf(dataList.get(element)); i < dataList.size(); i++) {

					if(dataList.get(element).satId == dataList.get(i).satId && 
							dataList.get(element).component.equals(dataList.get(i).component)) {
						
						long diff = dataList.get(element).time.getTime() - dataList.get(i).time.getTime();
						long diffInSec = diff/1000;
						if(diffInSec < 300) { //5 minutes in seconds
							
							dataList.get(element).counter++;
						}
						else {
							
							dataList.get(element).counter = 0;
							break;
						}
						
						if(dataList.get(element).counter >= 3) {
							
							String severity = "RED HIGH";
							if(dataList.get(element).component.equals("BATT")) {
								
								severity = "RED LOW";
							}
							
							Alert newAlert = new Alert(dataList.get(element).satId, 
									dataList.get(element).component, severity, dataList.get(element).time);
							
							newAlert.outputAlert();
							dataList.get(element).counter = 0;
						}
					}
				}
			}
		}
	}
}

