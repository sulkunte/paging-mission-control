import java.sql.Timestamp;

import com.google.gson.Gson;

public class Alert {

	protected int satelliteId;
	protected String component, severity;
	protected Timestamp timestamp;
	
	public Alert(int satId, String comp, String severity, Timestamp time) {
		
		satelliteId = satId;
		component = comp;
		this.severity = severity;
		timestamp = time;
	}
	
	public void outputAlert() {
		
		Gson gson = new Gson();
		System.out.println(gson.toJson(this));
	}
}
